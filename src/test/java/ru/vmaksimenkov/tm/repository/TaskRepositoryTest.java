package ru.vmaksimenkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import ru.vmaksimenkov.tm.AbstractTest;
import ru.vmaksimenkov.tm.dto.TaskRecord;
import ru.vmaksimenkov.tm.repository.dto.TaskRecordRepository;

public class TaskRepositoryTest extends AbstractTest {

    @Autowired
    private TaskRecordRepository repository;

    @NotNull
    private TaskRecord read() {
        @Nullable final TaskRecord taskRecord = repository.findByUserIdAndId(USER_ID, TASK.getId());
        Assert.assertNotNull(taskRecord);
        return taskRecord;
    }

    @Test
    public void clear() {
        repository.deleteAll();
        Assert.assertEquals(0, repository.count());
    }

    @Test
    public void save() {
        clear();
        repository.save(TASK);
        Assert.assertEquals(TASK.getName(), read().getName());
    }

    @Test
    public void update() {
        save();
        TASK.setName("New test task name");
        repository.save(TASK);
        Assert.assertEquals("New test task name", read().getName());
    }

}
