package ru.vmaksimenkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.crypto.password.PasswordEncoder;
import ru.vmaksimenkov.tm.AbstractTest;
import ru.vmaksimenkov.tm.model.User;

public class UserRepositoryTest extends AbstractTest {

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private UserRepository repository;

    @NotNull final User user = new User();

    @NotNull
    private User read() {
        @Nullable final User userRecord = repository.findByLogin("test");
        Assert.assertNotNull(userRecord);
        return userRecord;
    }

    @Test
    public void clear() {
        repository.deleteAll();
        Assert.assertEquals(0, repository.count());
    }

    @Test
    public void save() {
        clear();
        user.setLogin("test");
        user.setPasswordHash(encoder.encode("test"));
        repository.save(user);
        Assert.assertEquals(user.getLogin(), read().getLogin());
    }

    @Test
    public void update() {
        save();
        user.setFirstName("New test user name");
        repository.save(user);
        Assert.assertEquals("New test user name", read().getFirstName());
    }

}
