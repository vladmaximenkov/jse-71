package ru.vmaksimenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.vmaksimenkov.tm.AbstractTest;
import ru.vmaksimenkov.tm.dto.ProjectRecord;

import java.util.Objects;

public class ProjectRecordServiceTest extends AbstractTest {

    @Autowired
    private ProjectRecordService service;

    @NotNull
    private ProjectRecord read() {
        return Objects.requireNonNull(service.findByUserIdAndId(USER_ID, PROJECT.getId()));
    }

    @Test
    public void clear() {
        service.removeByUserId(USER_ID);
        Assert.assertEquals(0, service.count(USER_ID));
    }

    @Test
    public void save() {
        clear();
        service.merge(USER_ID, PROJECT);
        Assert.assertEquals(1, service.count(USER_ID));
        Assert.assertEquals(read().getName(), PROJECT.getName());
    }

    @Test
    public void update() {
        save();
        PROJECT.setName("Test project name 2");
        service.merge(PROJECT);
        Assert.assertEquals("Test project name 2", read().getName());
    }

}
