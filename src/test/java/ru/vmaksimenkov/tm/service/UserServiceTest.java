package ru.vmaksimenkov.tm.service;

import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.vmaksimenkov.tm.AbstractTest;
import ru.vmaksimenkov.tm.enumerated.RoleType;
import ru.vmaksimenkov.tm.model.User;

public class UserServiceTest extends AbstractTest {

    @Autowired
    private UserService service;

    @Autowired
    private PasswordEncoder encoder;

    @Test
    public void create() {
        service.createUser("Test user", "Test user", RoleType.USER);
        @Nullable final User user = service.findByLogin("Test user");
        Assert.assertNotNull(user);
        Assert.assertTrue(encoder.matches("Test user", user.getPasswordHash()));
    }

}
