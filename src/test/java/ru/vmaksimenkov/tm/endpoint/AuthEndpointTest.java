package ru.vmaksimenkov.tm.endpoint;

import lombok.SneakyThrows;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.vmaksimenkov.tm.AbstractTest;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class AuthEndpointTest extends AbstractTest {

    @Test
    @SneakyThrows
    public void login() {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/auth/logout"));
        mockMvc.perform(MockMvcRequestBuilders.get("/api/auth/login?username=test&password=test"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.success").value("true"))
                .andExpect(content().string("{\"message\":\"Success\",\"success\":true}"));
    }

    @Test
    @SneakyThrows
    public void user() {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/auth/user"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.username").value("test"))
                .andExpect(jsonPath("$.authorities[0].authority").value("ROLE_USER"));
    }

    @Test
    @SneakyThrows
    public void profile() {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/auth/profile"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.login").value("test"));
    }

    @Test
    @SneakyThrows
    public void session() {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/auth/session"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.authenticated").value("true"))
                .andExpect(jsonPath("$.authorities[0].authority").value("ROLE_USER"))
                .andExpect(jsonPath("$.principal.username").value("test"));
    }

}
