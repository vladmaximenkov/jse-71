package ru.vmaksimenkov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.vmaksimenkov.tm.AbstractTest;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ProjectEndpointTest extends AbstractTest {

    @Test
    @SneakyThrows
    public void post() {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/api/project")
                        .content(asJsonString(PROJECT))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        get()
                .andExpect(jsonPath("$.id").value(PROJECT.getId()))
                .andExpect(jsonPath("$.name").value(PROJECT.getName()));
    }

    @Test
    @SneakyThrows
    public void put() {
        post();
        PROJECT.setName("TestPost");
        mockMvc.perform(
                MockMvcRequestBuilders.put("/api/project")
                        .content(asJsonString(PROJECT))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        get()
                .andExpect(jsonPath("$.id").value(PROJECT.getId()))
                .andExpect(jsonPath("$.name").value(PROJECT.getName()));
    }

    @Test
    @SneakyThrows
    public void delete() {
        post();
        mockMvc.perform(
                MockMvcRequestBuilders.delete("/api/project/" + PROJECT.getId())
                        .content(asJsonString(PROJECT))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        get()
                .andExpect(jsonPath("$").doesNotExist());
    }

    @NotNull
    @SneakyThrows
    private ResultActions get() {
        return mockMvc.perform(
                MockMvcRequestBuilders.get("/api/project/" + PROJECT.getId())
                        .content(asJsonString(PROJECT))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

}
