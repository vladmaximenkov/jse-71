<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
    <head>
        <title>Task manager</title>
    </head>

    <style>
        body {margin: 0}
        .header {
            overflow: hidden;
            padding: 15px;
        }
        .header a {
            float: left;
            color: black;
            text-align: center;
            padding: 12px;
            text-decoration: none;
            font-size: 18px;
            line-height: 25px;
            border-radius: 4px;
        }
        .header a.logo {
            font-size: 25px;
            font-weight: bold;
        }
        .header a:hover {
            background-color: #ddd;
            color: black;
        }
        .header-right {
            float: right;
        }
        button {
            margin: 20px;
            background-color: #44c767;
            border-radius: 28px;
            border: 1px solid #18ab29;
            display: inline-block;
            cursor: pointer;
            color: #ffffff;
            font-family: Arial;
            font-size: 17px;
            padding: 16px 31px;
            text-decoration: none;
            text-shadow: 0px 1px 0px #2f6627;
        }
        button:hover {
            background-color: #5cbf2a;
        }
        button:active {
            position: relative;
            top: 1px;
        }
        h1 {
            padding-left: 15px;
        }
    </style>

    <body>

        <div class="header">
            <a href="/" class="logo">Task Manager
                <sec:authorize access="isAuthenticated()">
                    &nbsp;&nbsp;&nbsp;&nbsp;Welcome, <sec:authentication property="name"/>!
                </sec:authorize>
            </a>

            <div class="header-right">
                <a href="/">Home</a>
                <a href="/projects">ProjectTest</a>
                <a href="/tasks">TaskTest</a>
                <sec:authorize access="!isAuthenticated()">
                    <a href="/login">Login</a>
                </sec:authorize>
                <sec:authorize access="isAuthenticated()">
                    <a href="/logout">Logout</a>
                </sec:authorize>
                <sec:authorize access="hasRole('ADMIN')">
                    <a href="/admin">Admin page</a>
                </sec:authorize>
            </div>
        </div>

        <hr/>