package ru.vmaksimenkov.tm.exception;

public class StatusNotFoundException extends AbstractException {

    public StatusNotFoundException() {
        super("Error! Status not found...");
    }

}
