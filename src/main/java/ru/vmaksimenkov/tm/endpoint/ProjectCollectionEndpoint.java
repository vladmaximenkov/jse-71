package ru.vmaksimenkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.vmaksimenkov.tm.api.resource.IProjectCollectionResource;
import ru.vmaksimenkov.tm.dto.ProjectRecord;
import ru.vmaksimenkov.tm.service.ProjectRecordService;
import ru.vmaksimenkov.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping(value = "/api/projects", produces = MediaType.APPLICATION_JSON_VALUE)
@WebService(endpointInterface = "ru.vmaksimenkov.tm.api.resource.IProjectCollectionResource")
public class ProjectCollectionEndpoint implements IProjectCollectionResource {

    @NotNull
    private final ProjectRecordService service;

    @Autowired
    public ProjectCollectionEndpoint(@NotNull final ProjectRecordService service) {
        this.service = service;
    }

    @NotNull
    @Override
    @WebMethod
    @GetMapping
    public Collection<ProjectRecord> get() {
        return service.findAll(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping
    public void post(@NotNull @WebParam(name = "projects") @RequestBody final List<ProjectRecord> projects) {
        service.merge(UserUtil.getUserId(), projects);
    }

    @Override
    @WebMethod
    @PutMapping
    public void put(@NotNull @WebParam(name = "projects") @RequestBody final List<ProjectRecord> projects) {
        service.merge(UserUtil.getUserId(), projects);
    }

    @Override
    @WebMethod
    @DeleteMapping
    public void delete() {
        service.removeByUserId(UserUtil.getUserId());
    }

}
