package ru.vmaksimenkov.tm.api.resource;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.vmaksimenkov.tm.dto.ProjectRecord;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@RestController
@RequestMapping("/api/project")
public interface IProjectResource {

    @WebMethod
    @GetMapping("/{id}")
    ProjectRecord get(@NotNull @WebParam(name = "id") @PathVariable("id") String id);

    @WebMethod
    @PostMapping
    void post(@NotNull @WebParam(name = "project") @RequestBody ProjectRecord project);

    @WebMethod
    @PutMapping
    void put(@NotNull @WebParam(name = "project") @RequestBody ProjectRecord project);

    @WebMethod
    @DeleteMapping("/{id}")
    void delete(@NotNull @WebParam(name = "id") @PathVariable("id") String id);

}
