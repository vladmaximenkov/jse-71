package ru.vmaksimenkov.tm.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.vmaksimenkov.tm.dto.AbstractEntityRecord;

public interface AbstractRecordRepository<E extends AbstractEntityRecord> extends JpaRepository<E, String> {

}