package ru.vmaksimenkov.tm.repository;

import ru.vmaksimenkov.tm.model.AbstractBusinessEntity;

public interface AbstractBusinessRepository<E extends AbstractBusinessEntity> extends AbstractRepository<E> {

}