package ru.vmaksimenkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.vmaksimenkov.tm.model.Task;

import java.util.List;

@Repository
public interface TaskRepository extends AbstractBusinessRepository<Task> {

    @Modifying
    @Query("UPDATE Task SET projectId = :projectId WHERE userId = :userId AND id = :taskId")
    void bindTaskPyProjectId(@NotNull @Param("userId") String userId, @Nullable @Param("projectId") String projectId, @Nullable @Param("taskId") String taskId);

    @Nullable List<Task> findAllByUserIdAndProjectId(@NotNull @Param("userId") String userId, @Nullable @Param("projectId") String projectId);

    Long deleteByUserIdAndProjectId(@NotNull @Param("userId") String userId, @Nullable @Param("projectId") String projectId);

    Long deleteByUserIdAndProjectIdIsNotNull(@NotNull @Param("userId") String userId);

    @Nullable Task findByUserIdAndName(@NotNull @Param("userId") String userId, @NotNull @Param("name") String name);

    boolean existsByUserIdAndName(@NotNull @Param("userId") String userId, @NotNull @Param("name") String name);

    void deleteByUserId(@NotNull @Param("userId") String userId);

    void deleteByUserIdAndId(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    boolean existsByUserIdAndId(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Nullable List<Task> findAllByUserId(@NotNull @Param("userId") String userId);

    @Nullable Task findByUserIdAndId(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @NotNull Long countByUserId(@NotNull @Param("userId") String userId);

}
